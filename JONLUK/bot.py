"""
Twitter bot yelling all @JLMelenchon's tweets for fun purposes
"""


from tweepy import StreamListener, Stream, OAuthHandler, API

# OAuth access tokens
from secrets import consumer_key, consumer_secret
from secrets import access_token, access_token_secret

if consumer_key == 'EDIT ME':
    raise ImportError("Please configure OAuth in secret folder")

print("Authenticating to tweeter with OAuth")
# auth
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# API
api = API(auth)

# @JLMelenchon twitter id
jonluk_id = 80820758


# StreamListener class
class Jonluk(StreamListener):
    """
    MA PERSONNE EST SACRÉEE
    """
    def on_status(self, status):
        print("""
            Got {}
            From: {}
        """.format(status.text, status.user.screen_name))
        if (status.user.id == jonluk_id
            and (not hasattr(status, 'retweeted_status'))):
            yelled_status = "« {} »".format(status.text.upper())
            if len(yelled_status) > 221:
                yelled_status = """ « {} »

                #PoseTonMelenchon
                """.format(yelled_status)
            print('yelling: {}'.format(yelled_status))
            api.update_with_media('jonluk.jpg',
                                  status=yelled_status)

# main
if __name__ == '__main__':
    print("JONLUK is starting up...")
    JONLUK = Jonluk()
    stream = Stream(auth, listener=JONLUK)
    print("Getting timeline")
    stream.filter(follow=[str(jonluk_id)], async=True)
    print("JONLUK is started, waiting for @JLMelenchon's next tweet.")
